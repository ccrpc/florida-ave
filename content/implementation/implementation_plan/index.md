---
title: "Implementation of Final Recommendations"
draft: false
menu: 
weight: 10
---

The Florida Avenue Corridor Study Working Group recommends that roadway improvements on Florida Avenue between Lincoln Avenue and Vine Street include the following elements:

#### 1.	Add an off-street shared-use path on the south side of Florida Avenue between Lincoln Avenue and Race Street to improve safety and accessibility for pedestrians and cyclists.

Currently, the segment of Florida Avenue between Lincoln Avenue and Race Street is a substantial gap in the pedestrian and bicycle network, without any on-street or off-street bicycle facilities and no sidewalks on the south side of the street. Additionally, as discussed on the [Transportation Existing Conditions page](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/), the level of traffic stress for cyclists is high between Lincoln Avenue and Race Street and medium between Race Street and Vine Street. For pedestrians, the level of traffic stress fluctuates between medium and high. Public input included several respondents saying they avoid biking on Florida Avenue because it doesn’t seem safe, and others noted that they would use an off-street shared use path daily. 

The 2020 Urbana Pedestrian Master Plan and 2016 Urbana Bicycle Master Plan identify the gap in the pedestrian and bicycle network and recommend a ten-foot-wide concrete shared-use side path along the south side of Florida Avenue between Lincoln Avenue and Race Street (UBMP, p. 155, 227). Closing this gap from Lincoln to Race will create a nearly four-mile stretch of pathways and bike lanes on Florida Avenue that extends well beyond the study corridor (p. 155). 

The recommended path will connect existing east-west and north-south pedestrian and bicycle facilities on the surrounding streets, as well as the existing shared use paths to the south and west of Lincoln Avenue, reducing stress on the network and improving user safety and accessibility.

#### 2.	Upgrade lighting throughout the corridor to improve safety and accessibility for all users.

Florida Avenue has consistent street light coverage between Busey Avenue and Race Street on both the north and south sides of the roadway. However, between Lincoln Avenue and Busey Avenue there is lighting only on the south side of the street, and the segment between Race Street and Vine Street includes lighting only on the north side of the street. Research has shown that poor lighting can cause a significant decrease in the number of people walking and biking. Poor lighting can also reduce the number of transit riders, since transit riders must walk to and from bus stops.

Therefore, upgrading the lighting throughout the corridor will improve accessibility and visibility for all users. Lighting improvements should consider the existing lighting in and around the corridor as well as proposed lighting improvements planned by the University of Illinois along Kirby and Florida Avenue west of Lincoln Avenue. 

The University’s proposed lights and other lighting in the area are more modern in design when compared to the West Urbana Neighborhood existing lighting infrastructure. According to responses from the public surveys, respondents worry that upgraded lighting will diminish the neighborhood’s historic character. When implementing lighting upgrades, consideration should be given to creating a cohesive lighting scheme throughout Florida Avenue, while preserving the character of the neighborhood. Lighting should also be dark sky compliant to minimize light pollution.



#### 3.	Replace damaged sidewalk panels throughout the corridor to improve pedestrian safety and accessibility.

As previously stated, the segment of Florida Avenue between Lincoln Avenue and Race Street is a substantial gap in the pedestrian and bicycle network, without any on-street or off-street bicycle facilities and no sidewalks on the south side of the street. Replacing damaged sidewalk panels will promote connectivity, safety, and accessibility for pedestrians throughout the corridor and surrounding streets. Cracking and uneven surfaces along the sidewalk put pedestrians at risk for tripping and falling and pose as a hazard to cyclists and wheelchair users ([FWHA](https://safety.fhwa.dot.gov/ped_bike/tools_solve/fhwasa13037/chap5.cfm)). Some sections of the existing sidewalk exhibit high levels of disrepair and would require longer stretches of the sidewalk to be replaced. 

Public input strongly supported this implementation. Approximately 89% of survey respondents supported the proposal, the highest level of support among respondents. Additionally, public feedback from the Urbana Pedestrian Master Plan (2020) showed that people avoided walking along the corridor due to poor sidewalk conditions. 

#### 4.	Reconstruct the Florida Avenue roadway from Lincoln Avenue to Vine Street to improve safety and accessibility for existing vehicle lanes and on-street bike lanes (including crosswalk repainting and sidewalk ramp reconstruction).

The existing Florida Avenue roadway conditions are very poor, averaging a 31 out of 100 on the Pavement Condition Index (PCI). Common issues affecting the roadway are fatigue and block cracking, rutting, and patching. Given the poor pavement conditions, reconstructing the roadway is an important step in improving the safety of all users. If left to deteriorate, these conditions pose a danger to motorists using the corridor and would lead to more costly repairs in the future. Public feedback reiterated the poor roadway conditions and desire for improvement. 

#### 5.	Add bus cut-outs to prevent traffic obstruction at the Florida Avenue intersections with Orchard Street (on the SE Corner) and Vine Street (on the NW corner, at Blair Park). Cut-outs will include concrete landing pads and shelters for bus riders.
Buses are an excellent and inexpensive option for residents and visitors traveling around the Champaign-Urbana community. The rates of people using transit to get to work are higher in Urbana and the corridor study Area of Influence compared with the rest of the region.  Currently, the Florida Avenue corridor accommodates four different bus routes, 7-days a week: the Bronze, Raven, Red, and Teal lines. The intersections at Orchard and Vine have two of the highest ridership numbers along the corridor.

Public input documented concern that buses contributed to congestion along the study corridor since the roadway is too narrow for private vehicles to pass while buses are stopped. Adding bus cut-outs can help alleviate increased congestion along the corridor by allowing vehicles to bypass bus stops, as well as provide a safer experience for both motorists and transit passengers. 

#### 6.	Add a parking lane on the north side of Florida Avenue between Busey Avenue and Race Street (within the existing roadway width, no pavement expansion needed).

A parking lane used to exist on the north side of Florida Avenue between Busey Avenue and Race Street. This lane was removed between 2015 and 2017 due to maintenance issues, with the understanding that it would be reinstalled at some point in the future when the entire roadway surface could be improved. Comments from Phase One of public feedback included an understanding by nearby residents that the parking lane would be returned when feasible, and these residents were interested in bringing it back. The parking lane was also mentioned in the 2016 Urbana Bike Master Plan, which notes that the nature of the roadway creates the need to keep on-street parking for area residents, while this section’s limited width and high traffic volumes make it a less than ideal location for on-street bicycle lanes ([Urbana Bicycle Master Plan](https://www.urbanaillinois.us/bicycle-master-plan) page 155). While providing parking for adjacent residents, an on-street parking lane in this location can also benefit transportation flow in the following ways: 
- by reducing the navigable roadway width and thereby serving as a traffic calming device to slow vehicle travel speed.
- by providing dedicated space for private vehicles as well as delivery or service vehicles accessing adjacent driveways/properties along Florida Avenue.
- by functioning as an additional buffer between vehicle traffic and the adjacent sidewalks and properties on the north side of the street.

#### 7.	Upgrade existing traffic signals (including crosswalk signals) at the Florida Avenue intersections with Lincoln Avenue and Orchard Street to improve functionality.

In 2045, all four major intersections in this corridor are projected to be operating at a level of service **D, E, or F** (worst) in the AM and/or PM peak periods if no transportation improvements are made. 

For currently signalized intersections, improvements mean retaining and upgrading the existing traffic signals. It is recommended that traffic signals at the Lincoln Avenue and Orchard Street intersections are upgraded to include accessible (audible) pedestrian signals and push-button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary. This will not only benefit motorists by increasing traffic flow but will also provide a safer network for pedestrians and cyclists.

	
#### 8.	Add new traffic signals (including crosswalk signals) at the Florida Avenue intersections with Race Street and Vine Street to improve safety and traffic flow.	

Adding new traffic signals will work to improve the traffic flow along the study corridor, especially at problem intersections. The intersection of Race Street and Florida Avenue is currently an all-way stop operating at a level of service **D** in the AM peak period and level of service **F** (worst) in the PM peak period. In 2045, all four major intersections are projected to be operating at a level of service **D, E, or F** (worst) in the AM and/or PM peak periods if no transportation improvements are made. As discussed on the [Transportation Existing Conditions page](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/), the intersection of Race Street and Florida Avenue had the highest number of injury crashes in the corridor, even though the intersection of Lincoln Avenue and Florida Avenue carries nearly twice the amount of traffic. This rate of crashes placed the intersection of Race and Florida as a priority for safety improvements in the IDOT District in which Urbana is located. With no improvements, the safety conditions will likely deteriorate further and delay time per vehicle at these intersections will increase to as much as over 80 seconds. 

Upon [evaluating numerous scenarios](https://ccrpc.gitlab.io/florida-ave/future-scenarios/scenario-evaluation/#scenario-selection), signaling at each of the four major intersections looks to greatly improve the level of service ratings along the corridor. It is recommended that new traffic signals be installed at the Florida Avenue intersections with Race Street and Vine Street and include accessible (audible) pedestrian signals and pushbutton devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary. These signals will not only benefit motorists by better managing traffic flow but will also provide a safer network for pedestrians and cyclists. If correctly configured and implemented to include pedestrian safety measures (pushbutton devices and signal heads following MUTCD recommended placement, appropriately timed walkways and crosswalks,  auditory signaling and clear signage), signalized intersections help to improve pedestrian safety ([IDOT](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiI_5yA_pv5AhUuj4kEHevGC84QFnoECAYQAQ&url=https%3A%2F%2Fidot.illinois.gov%2FAssets%2Fuploads%2Ffiles%2FTravel-Information%2FSpecialty-Lists%2F06DPS-SignalizedIntersections%2520(Springfield).pdf&usg=AOvVaw3PEWkdEEPZeOWh8TDGT9rx)). 

#### 9.	Consolidate bus stops between Lincoln Avenue and Vine Street to improve route efficiency by reducing the total number of bus stops in the corridor from 14 to 12 (including new stops with bus cut-outs on the NW corner of the Florida Avenue and Vine Street intersection at Blair Park).	

Several of the 14 existing bus stops within the corridor are not frequently utilized. Consolidating the existing bus stops and reducing the total number of stops can increase route efficiency and improve transit travel times by limiting the frequency at which a bus must stop along the corridor. This can also have positive effects on the traffic flow for all users throughout the corridor, resulting in fewer delays. Public input documented concerns that buses contribute to roadway congestion, as vehicles are required to stop and wait for the bus at each of its stops. Each of the removed stop locations will still feature either existing or new stops within 600 feet or 2 blocks from the previous stop locations, and stop distances will be consistent with, for example, the MCORE bus corridor on White Street in Champaign. 

#### 10.	Add two-stage turn queue boxes at the Florida Avenue intersection with Race Street to help cyclists transition between on-street bike lanes and off-street shared-use paths.

Existing bike lanes are located south of the corridor along Orchard Street and within the corridor between Race and Vine. Additionally, off-street shared use paths exist south of the corridor along Race Street, west of Lincoln Avenue along Florida Avenue, and south along Lincoln Avenue. Adding two-stage turn queue boxes will help cyclists safely make left turns and transition to and from these existing bicycle facilities along the corridor. With the addition of the off-street shared path on the south side of Florida Avenue between Lincoln Avenue and Race Street, bicyclists will need a safe way to transition from the current bike lane (from Race to Vine) to the shared use path. Two-stage turn queue boxes were also mentioned as a recommendation in the 2016 Urbana Bicycle Master Plan at the northwest and southwest corners of the Florida Avenue intersection with Race Street ([Urbana Bicycle Master Plan](https://www.urbanaillinois.us/bicycle-master-plan) page 277).

Public input documented mixed opinions regarding the turn queue boxes, stating that they will cause confusion for both motorists and cyclists. While these boxes may be new to many residents, they do already exist in several locations in our community, and the [City of Urbana](https://www.youtube.com/watch?v=kZ4X6_T1K1U) and [MTD have provided helpful resources](https://mtd.org/inside/mtd-pulse/how-to-use-a-bike-box-stop-turn-wait/) on how to safely utilize them. If implemented, continued public education on the use of the boxes should be promoted. 

### Timeline

Ideally, all corridor improvements would happen concurrently in order to maximize any materials and labor efficiencies. Concurrency would minimize redundant work, such as repainting road surfaces that are resurfaced shortly thereafter or resurfacing the roadway only for it to be torn up to install new infrastructure. However, the timeline for implementing individual components of the corridor improvement will be dependent on the availability of funding.

As discussed previously, the top safety priority in the corridor is the Florida Avenue intersection with Race Street, based on crash frequency and severity. Although traffic levels and crash frequency are not currently as severe at the other three main intersections along the corridor, future traffic modeling suggests that, with the current infrastructure, several other intersection approaches throughout the corridor will be declining and potentially failing by 2040 during both morning and afternoon peak hours. This is why construction efficiencies and intersection consistency along the corridor makes concurrent implementation preferable even though current safety and traffic conditions may not present the same level of urgency at every roadway segment or intersection along the corridor.

Depending on funding availability, the ten separate recommendations are organized in general order of priority below:
 
#### Short Term
- Reconstruct the intersection of Florida Avenue and Race Street
  - Reconstruct pavement
  - Add new traffic signals (include accessible (audible) pedestrian signals and pushbutton devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority)
  - Add two stage turn queue boxes for cyclists
- Construct an off-street shared use path on Florida Avenue between Lincoln Avenue and Race Street
- Consolidate bus stops

#### Medium Term
- Improve pavement conditions on Florida Avenue between Lincoln Avenue and Vine Street
- Add a parking lane within the existing roadway width between Busey Avenue and Race Street
- Construct two bus cut-outs along Florida Avenue to prevent traffic obstruction, include concrete landing pads to improve sidewalk connections and shelters for riders
- Replace damaged sidewalk panels
- Upgrade lighting throughout the corridor

#### Long Term
- Upgrade existing traffic signals at the Florida Avenue intersections with Lincoln Avenue and Orchard Street (include accessible (audible) pedestrian signals and pushbutton devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority)
- Add new traffic signals at the intersection of Florida Avenue and Vine Street (include accessible (audible) pedestrian signals and pushbutton devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority)

### Potential Funding Options

#### STBGP- Surface Transportation Block Grant Program

The Surface Transportation Block Grant Program (STBGP), previously known as Surface Transportation Program (STPU), is one of the core Federal-aid Highway Program categories. The Champaign-Urbana urban area receives a population-based suballocation of STBGP funds from IDOT each year. 

Local agencies seeking to use STPU/STBGP funds for the Champaign-Urbana Urbanized Area are required to submit a project application to be reviewed by CUUATS staff and the Project Priority Review working group who use a set of criteria based on federal and regional transportation goals to score each project.

The City of Urbana has secured nearly $4 million in funding for fiscal year 2024 for improvements in the Florida Avenue corridor through a combination of STBG funding, local funding, and COVID relief funding.

#### RAISE Discretionary Grant Program- Rebuilding American Infrastructure with Sustainability and Equity

Formerly known as BUILD (Better Utilizing Investments to Leverage Development) and TIGER (Transportation Investment Generating Economic Recovery) Discretionary Grant Programs, RAISE provides grant funding to projects that advance national goals and will have a significant impact locally or regionally. 

MTD and the City of Urbana submitted an application in 2022 for RAISE grant funds to fund comprehensive multi-modal improvements throughout the Florida Avenue corridor. Although the application was not selected for funding in 2022, MTD and/or the City can reapply again in the future. For more information, visit the U.S. Department of Transportation [webpage on RAISE](https://www.transportation.gov/RAISEgrants). 

#### BIL- Bipartisan Infrastructure Law

The Bipartisan Infrastructure Law (BIL) [also known as the Infrastructure Investment and Jobs Act (IIJA)] was signed into law in November 2021 by President Biden. The law “provides approximately $350 billion for Federal highway programs over a five-year period (fiscal years 2022 through 2026). Most of this funding is apportioned (distributed) to States based on formulas specified in Federal law,” as well as through grant programs ([Bipartisan Infrastructure Law | Federal Highway Administration](https://www.fhwa.dot.gov/bipartisan-infrastructure-law/funding.cfm)). 

The Multimodal Projects Discretionary Grant Opportunity (MPDG) allows agencies to apply for three grant programs (INFRA, RURAL, and MEGA) receiving funding from the BIL using one application. 
- [INFRA- Infrastructure For Rebuilding America](https://www.transportation.gov/grants/infra-grants-program)
- [RURAL- Rural Surface Transportation Grant Program](https://www.transportation.gov/grants/rural-surface-transportation-grant)
- [MEGA- National Infrastructure Project Assistance](https://www.transportation.gov/grants/mega-grant-program) 

#### Illinois HSIP- Highway Safety Improvement Program 

The HSIP is a federal program managed by the Federal Highway Administration (FWHA) and aims to reduce fatalities and serious injuries from traffic-related crashes occurring on public roads. Funding is allocated to the State of Illinois and then set aside in the Highway-Railroad Crossing Fund and the High-Risk Rural Roads program. The remaining funds are allocated to the HSIP- Roads fund comprised of HSIP- State Roads, which receives 60% of the remaining funds, and HSIP- Local Roads, which receives 20% of the funds. The HSIP is a possible funding source specifically for improvements at Florida Avenue intersection with Race Street due to its IDOT designation as a top 5 percent safety priority. For more information on HSIP and the funding process, visit the [IDOT webpage on HSIP](https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-opportunities/highway-safety-improvement-program).

#### ITEP- Transportation Enhancement Program 

According to the Illinois Department of Transportation, the ITEP is a “reimbursable grant program” that requires joint funding, with the preliminary engineering costs paid up-front and reimbursed over the implementation process. Federal funding reimburses up to 50% for any acquisition costs and up to 80% for preliminary engineering costs. Project categories include pedestrian/bicycle facilities, making this a feasible option to fund the off-street shared use path on the south side of Florida Avenue between Lincoln Avenue and Race Street. More information can be found at the [IDOT webpage on ITEP](https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-opportunities/ITEP).

#### Other Grants

Information on additional grants can be found here: [Grants | US Department of Transportation](https://www.transportation.gov/grants). 
