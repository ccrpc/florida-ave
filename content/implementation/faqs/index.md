---
title: "Frequently Asked Questions"
draft: false
menu: 
weight: 20
---

Some concerns or questions were expressed frequently by members of the public throughout the Florida Avenue Corridor Study process. This page seeks to more directly address those recurring questions, providing additional context or insight into the corridor study process to help the public understand the rationale for the study recommendations. 

#### Florida Avenue is a neighborhood, residential road, why does this study accommodate through traffic on this road?

While the land use on this stretch of Florida Avenue is primarily residential, it is nonetheless a major east-west corridor in the area, the only such corridor for a mile in either direction, and has been for more than fifty years, since Florida and Kirby Avenues were connected. As discussed in the [Existing Conditions – Transportation]( https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/) page, Florida Avenue is a minor arterial, meaning that it serves “shorter trips between traffic generators within urban areas” and “has more access but a lower level of mobility than interstates and other principal arterials.” By definition, this distinguishes its use and purpose from the local roads that surround it. 

The neighborhoods surrounding Florida Avenue are designed with features—like culs-de-sac, brick roads, and discontinuous grids—that discourage through traffic within these neighborhoods. When those features are implemented, traffic is directed elsewhere, and in this case, one of the primary locations that these neighborhoods push traffic toward is Florida Avenue.

Some residents have suggested that drivers intending to use Florida Avenue for through-traffic should use Windsor instead, but this ignores the fact that, for the vast majority of Urbana residents and destinations, Windsor is out of the way of the path of travel, (as mentioned previously, up to a mile out of the way). 

#### Are the recommendations of this study looking to increase car traffic on this corridor?

No, the goal of the corridor study recommendations is not to increase vehicle traffic on the Florida Avenue Corridor. These recommendations are provided with the goal of accommodating likely future trends in our community and along this transportation corridor. As noted on the [home page](https://ccrpc.gitlab.io/florida-ave/) for the study website and on the [Transportation page](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/) in Existing Conditions, the primary impetus for this study was the frequency of vehicle crashes along this corridor. The corridor study has sought to address this problem, while also addressing the needs of all other corridor users and stakeholders. Given the data that is available and projections for regional population change, RPC staff believes that traffic will continue to increase on this corridor in the coming decades, leading to a further decline in roadway conditions. Our recommendations look to counteract this and provide a safe and efficient transportation system for all area residents and all modes of travel, including pedestrians, bicyclists, bus passengers, and people in vehicles. 

Encouraging residents to utilize modes like public transit will require this transit—which runs on the same roadways and sits in the same traffic as private vehicles—to be able to navigate the corridor smoothly and safely, which requires maintaining a high level of service. This study is not recommending expanded roadway infrastructure, but more efficient use of the roadways already present. Respondents have proposed inventive ideas to reduce the role of private automobiles in our area, including disallowing cars from the urban center, implementing bus priority, or creating a bus rapid transit system; however, these ideas are beyond the scope of this study.

Some respondents were concerned about the goal to move automobile traffic through the corridor more quickly, but reducing automotive delay (time spent stuck at intersections) is not the same as trying to increase the actual speed that drivers travel. Coordinated signals along the corridor can be programmed to provide a ["green wave"](https://en.wikipedia.org/wiki/Green_wave), where signal timing encourages vehicles to pass through the corridor at the proper moderate speed. Traffic calming improvements—like adding a parking lane and shrinking the navigable road space—will allow the Florida Avenue Corridor to move users along the corridor more efficiently while still ensuring they maintain safe speeds.  Reducing the time spent waiting at intersections will also reduce the emissions from vehicles travelling through this corridor.

Ultimately, shifting area residents to more sustainable modes of transportation is a large-scale goal. While this study can be an incremental step in that direction, actions from this study should be in line with overall goals for the transportation system; like the Urbana Bike Plan, which this study’s recommendations build upon. Neglecting serious deterioration in roadway level-of-service and counting on residents to figure out other ways to travel is not a systematic and thought-out approach to addressing this issue.  Improving the way that this corridor functions for motor vehicles does not have to come at the expense of other modes, and this study can improve road function for all users while local residents and officials look for more ways to reduce automobile emissions.

#### Many roadway users are unfamiliar with the proposed two-stage turn queue boxes. Won’t this serve as a hazard as roadway users are confused by them?

While these boxes may be new to many residents, they do already exist in several locations in our community, and [local agencies have provided helpful resources](https://mtd.org/inside/mtd-pulse/how-to-use-a-bike-box-stop-turn-wait/) on how to safely utilize them. Ultimately, these boxes provide a means for bicyclists to turn left without having to navigate in front of moving automobile traffic. Cyclists who are familiar with the boxes and prefer them can use them, while those who prefer to continue to use the left turn lane can do so. 

#### Isn’t the proposed multi-use path inconvenient for cyclists traveling west on Florida—requiring them to navigate from a bike lane on the north side of the street to the path on the south?

Some respondents have noted that the transition at Race Street—from bike lanes east of Race to the proposed multi-use path west of Race—would be an indirect route for westbound cyclists, and continuing the bike lanes west of Race would be preferable. In addition to the previously-stated rationale for adding parking along the north side of Florida Avenue here, directing cyclists onto the multi-use path is also preferable due to safety concerns. The proposed route directs cyclists to navigate from the roadway to the multi-use path at Race and Florida. If bike lanes were continued, cyclists would be required to navigate the intersection at Lincoln—a significantly larger and more heavily-traveled intersection—by merging into traffic when the bike lane ends, and navigating the intersection from the roadway. Instead, riding on the multi-use path allows cyclists to connect directly to the existing multi-use path infrastructure west of Lincoln, either continuing to travel west on the path along Florida/Kirby Avenues, or travelling north to campus on the path along Lincoln Avenue.

#### Will the proposed traffic signals detract from the residential character of the neighborhood?

While the recommended traffic signals would be different from the current infrastructure along the Florida Avenue Corridor, they are not antithetical to the residential nature of many of the land uses along the corridor. Traffic signals are already used in Urbana on corridors similar to Florida Avenue – roadways on the edges of neighborhoods. Traffic signals can also help to address neighborhood impacts from these intersections, including emissions and noise from vehicle stopping and starting required at the current stop-controlled intersections.

Some residents have become concerned about the installation of “standard highway” lighting and signals along the corridor, but this is not language that was used during the corridor study, or language used in transportation planning generally. The City of Urbana will install infrastructure that is appropriate for the locations and consistent with the other similar contexts around the community.

#### Does the proposed conversion from all-way stops to signalized intersections introduce new safety concerns?

One of the primary impetuses for this study was the danger that the intersection of Race Street and Florida Avenue currently poses; with 25 crashes, and 8 resulting in injury, between 2014 and 2018. While respondents have noted research showing all-way stop intersections have lower crash rates than signalized intersections, every intersection is unique and the current recommendations are based on many variables, including existing evidence and particular local circumstances and needs. Research also suggests that conversion from an urban stop-controlled intersection can reduce [all crashes by 23%](https://www.cmfclearinghouse.org/detail.cfm?facid=319) and [angle crashes by 67%](https://www.cmfclearinghouse.org/detail.cfm?facid=320). Safety concerns about this conversion can also be addressed through complementary interventions along the corridor—like returning the former parking lane between Busey and Race, which will narrow the navigable roadway and reduce driver speeds.

Some respondents noted the stress and fear of navigating the current stop-controlled intersection as either a driver or pedestrian, due to up to nine drivers at a time seeking to coordinate right of way, which can lead to the overlooking of smaller pedestrians and bicyclists. Modern signals can be programmed to provide timely and safe passage for bikes and pedestrians, while helping to ensure that all road users are aware of right-of-way and proceeding at a safe time.

Some respondents favored installation of roundabouts along this corridor as their preferred intersection treatment. As part of the corridor study process, staff explored the benefits and feasibility of roundabouts for this corridor. However, for the reasons described in the [“Scenario Selection” section on the Scenario Evaluation page](https://ccrpc.gitlab.io/florida-ave/future-scenarios/scenario-evaluation/#scenario-selection), this intervention was not viewed as preferable at the intersections of this corridor.

#### Are you proposing any changes to the eastbound merge lane at Lincoln Avenue?

{{<image src="lincoln_merge.PNG"
  alt="Image of merge lane on Florida Avenue just east of Lincoln Avenue"
  caption= "Florida Avenue and Lincoln Avenue Intersection, 2021" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="right">}}

Public feedback included complaints and safety concerns about the narrowing of Florida Avenue when traveling eastbound at the Lincoln Avenue intersection. Though the existing merge lane is technically compliant with transportation standards, respondents noted that the merging of through traffic from two lanes to one immediately after the intersection was a source of confusion and crashes. Current traffic volumes and projected future traffic volumes do not warrant adding vehicle lanes on Florida Avenue east of Lincoln Avenue or reducing vehicle lanes on Florida Avenue west of Lincoln Avenue, which means traffic will continue to need to merge in the vicinity of the intersection of Florida Avenue and Lincoln Avenue. Staff investigated the possibility of extending the merge lane in its current location, however existing access driveways limit that option. Staff also investigated the possibility of changing the eastbound right/through lane into an exclusive right-turn lane and merging eastbound through traffic into one lane before the intersection. However, this option was ultimately rejected for a couple of reasons. The primary reason is that forcing all through traffic into one lane before the intersection could cause long queues when traffic is stopped at Lincoln Avenue during peak hours, possibly blocking traffic at other access points west of Lincoln Avenue. In addition, eastbound right-turn traffic is not high enough to warrant a right-turn-only lane. Finally, while the post-intersection merge may be a cause of stress and near-misses, crash reports do not indicate frequent collisions at this location (just two no-injury crashes were recorded between 2014 and 2018). While relocating the merge is not being recommended at this time, city staff is considering options to improve signage and/or road markings in this area, to improve driver awareness.

#### Why don’t you simply reduce the pavement width of Florida Avenue between Busey and Race?

{{<image src="buffer_image.PNG"
  alt="Image of striped buffers painted on Florida Avenue between Busey Avenue and Race Street"
  caption= "Striped buffers on Florida Avenue between Busey Avenue and Race Street, 2021" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="right">}}

Between Busey Avenue and Race Street, Florida Avenue is currently comprised of a traffic lane in each direction, with narrow, painted buffers along each curb. These buffer areas taper away at intersections, in order to make room for additional turn lanes. Because these buffers are less than seven feet at the widest, they cannot be used for vehicle storage or travel. They are also not painted as bike lanes and are therefore not safe for cyclists. To make more efficient use of the pavement, the Working Group considered narrowing the pavement along this section, providing two traffic lanes without the extra pavement space. This would reduce the road maintenance burden for the city and create more permeable green space along the corridor. However, instead, the study is recommending that the current roadway width be maintained, and a parking lane be created along the north side of the road. Prior to the last decade, the roadway section here featured a vehicle parking lane along the north side of the road, with the two traffic lanes shifted to the south. This lane was removed between 2015 and 2017 due to the burdens of maintenance, with property owners along the corridor understanding that it would be returned when feasible. Because of the long block length along this section, returning this parking lane will provide additional parking for adjacent residents as well as for delivery trucks or other commercial vehicles that currently block the flow of traffic along this stretch. The parking lane will also provide some of the benefits of reduced pavement space, including traffic calming and additional buffer width between pedestrians and automobile traffic.

{{<image src="aerial.jpg"
  alt="Aerial images of roadway with different roadway painting in 2020 and 2014"
  caption= "Aerial images of Florida Avenue between Orchard Street and Carle Avenue from 2020 (left) and 2014 (right)" 
  attr="CCGIS"
  position="full">}}

#### Can you do anything about the westbound turn lane inconsistencies at the Race Street and Vine Street intersections?

When traveling westbound on Florida Avenue, through traffic is directed to the left lane at the Vine Street intersection, while through traffic is directed to the right lane at the Race Street intersection. This inconsistency can lead to some driver confusion, as documented in the public input. Although specific intersection design is beyond the scope of this study, the City of Urbana is currently overseeing a separate intersection design study to consider this discrepancy and other intersection needs.

{{<image src="turn_lane.jpg"
  alt="Examples of turn lanes on Florida Avenue"
  caption= "Florida Avenue at Race Street (left) and Vine Street Intersection (right), 2021" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

