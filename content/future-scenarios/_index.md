---
title: "Future Scenarios"
draft: false
menu: main
weight: 30
---
As we gather data and public sentiment, this section will feature possible scenarios for addressing transportation issues along Florida Avenue. 

In the meantime, the two pages linked below—Relevant Plans and Scenario Evaluation—provide information on existing plans relevant to this corridor, as well as the baseline future predictions for the corridor, absent any substantial infrastructure changes.