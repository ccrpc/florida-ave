---
title: "Public Outreach - Phase Three Summary"
draft: false
weight: 16
---

Phase 3 of public outreach was open from Monday, September 19th to Monday, October 3rd. This outreach consisted of a presentation to Urbana City council on the completed draft of the study, including implementation plans, followed by two weeks of public comment gathered through an online survey and email. The following methods were used to make community members aware of the website and survey:

-	Posting on the CUUATS Facebook, Instagram, and Twitter
-	Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include: CCRPC Communications office, CUMTD, City of Urbana, Urbana Parks District, PACE, The SUNA and WUNA neighborhood groups, UIUC Facilities, and UIUC University Housing.
-	Installing yard signs in the public right of way along the Florida Avenue corridor

The survey structure for this phase of feedback was minimal, with one open-ended question – “Please provide any feedback on the Florida Avenue Corridor Study recommendations below.”—followed by an option to provide a contact email. This outreach resulted in 32 survey responses, along with four emails providing feedback.

## Coded Comments

The survey responses and emails were sorted into non-exclusive themes (the same comment can appear in multiple themes), as seen below. These themes indicate that the most commonly expressed sentiments were the need to ensure bicyclist and pedestrian safety, opposition to converting the four-way stops on the corridor to signalized intersections, appreciation for the study writ large (even if commenters took issue with individual recommendations), and support for deprioritizing and calming automobile traffic along the corridor. Previous responses to many of these concerns can be found in the Implementation section, and especially the [Frequently Asked Questions page](https://ccrpc.gitlab.io/florida-ave/implementation/faqs/).

<rpc-table url="sorted_comments.csv"
  table-title="Phase Three Comment Themes"
  text-alignment="l,c"></rpc-table>


#### "Other" Categories (Less than 4 comments each)
-	Bus Infrastructure Concerns
- Website/Report Structure Suggestions
-	Pro-Signal
-	Pro-Dark Sky Compliant Lighting
-	Pro-New On-Street Bike Lane
-	Anti-Turn Queue Box

## Comments and Suggestions to Consider

In the text of their comments, community members raised specific points, including particular suggestions or ideas for implementation that may be useful when moving forward with projects on the Florida Avenue Corridor or elsewhere. Specific suggestions within the primary themes from the table are found below  - 

#### Ensure Bike and Pedestrian Safety
-	Vehicles at intersections need to be made to decelerate and watch for bikes and pedestrians
-	At the current four-way stops, remove the turn lanes and provide shorter crossing distances for pedestrians
-	Due to vehicle levels, an on-road bike lane here is too dangerous
-	Bike lane design should consider the safety risk of delivery drivers and other stopped vehicles ignoring on-street bike lanes
-	Make sure the road is being maintained and cleaned so that on-street bike lanes are safe
-	Make sure that the adjacent bike lane and proposed bus cut-out at Blair Park are designed for the safety of all transportation modes 

#### Anti-Signal
-	Skeptical of the need for signalized intersections on this corridor
-	Corridor improvements should not encourage vehicle traffic
-	Traffic signals should be managed well to prevent delay at peak hours; they are not always managed well
-	Corridor improvements should encourage drivers to travel at moderate speeds

#### Thanks/Looks Good
-	Explore ways to accelerate the implementation of the study recommendations (including securing funding to perform construction work more quickly)

#### Deprioritize Vehicle Traffic
-	When doing future traffic modelling, explore methods to include other non-automobile modes in models
-	Given the limited scope of vehicle level-of-service data, future analysis should prioritize other modes and data points that capture other important information about how the corridor functions

#### Institute Traffic Calming
-	Reduce the speed limit along this corridor
-	Include cameras on newly-installed signals
-	If implementing signals, include no-right-on-red rules for pedestrian safety
-	Use speed bumps or other physical designs to reduce speeds

#### Pro-Roundabout
-	To make roundabouts feasible at a future date, the City could plan to acquire intersection-adjacent properties on the market in the future, at a cheaper cost, rather than going through the imminent domain process
-	Consult case studies like Carmel, Indiana to see how cities implement roundabouts and educate residents on how to use them
-	Even though they cost more up front, lower ongoing costs make roundabouts preferable to signals

#### Preserve Neighborhood Character
-	If signals are implemented, ensure that residents are able to safely get out of their driveways
-	Include greenery buffers as a way to prevent noise pollution from vehicle traffic
-	Make sure any lighting changes reduce light pollution along the corridor

#### Anti-Parking Lane
-	Concerned about safety of people getting into and out of parked cars along the roadside
-	The road space allocation toward the proposed parking lane would be better used as a bike path or expanded sidewalk

#### Pro-Protected Bike Lane
-	Adding a protected bike lane west of Race Street would prevent westbound cyclists from having to cross the road to access the bike path
-	Consider how to provide physical separation for the existing bike paths east of Race Street


