---
title: "Public Involvement Info"
draft: false
weight: 5
bannerHeading: "Get Involved!"
bannerText: >
  No one knows the community like those who live, work, and go to school here. Get involved in Florida Avenue Corridor Study to share your input! 
---


{{%sidebar title="" position="top" %}}

### Share Your Input!

*Thank you to everyone who took the surveys and provided feedback. Overall, more than __500__ survey
responses were collected!* 

{{%/sidebar%}}

The Champaign-Urbana Urbanized Area Transportation Study (CUUATS) is evaluating one mile
of Florida Avenue in Urbana (from Lincoln Avenue to Vine Street) in partnership
with the City of Urbana, UIUC, and MTD. The goal of this study is to identify
ways to increase transportation safety and mobility in this high-traffic
corridor. Your input will inform the future vision for Florida Avenue and help
increase mobility in the Champaign-Urbana area. 

Under the Implementation tab at the top of this page, community members will find pages describing the [Implementation of the Preferred Scenario](https://ccrpc.gitlab.io/florida-ave/implementation/implementation_plan/), where the recommended changes to the corridor, the reason for these changes, and strategies for achieving these changes are discussed. The Implementation section also contains a [Frequently Asked Questions page](https://ccrpc.gitlab.io/florida-ave/implementation/faqs/), where CUUATS staff have attempted to address common questions or concerns that members of the public have raised throughout this process.

Community members interested in the entire corridor study process that led to these recommendations can explore the [existing conditions](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/),
previous [public input](https://ccrpc.gitlab.io/florida-ave/public-involvement/survey-results/) on the process, [the future scenario evaluation](https://ccrpc.gitlab.io/florida-ave/future-scenarios/scenario-evaluation/) that staff performed to arrive at their recommendations, and a mode-by-mode description of the [benefits of these recommendations](https://ccrpc.gitlab.io/florida-ave/future-scenarios/future-recommendations/). 

Members of the public who are new to this project can also watch the two videos below from previous rounds of public input, which describe the corridor study process and the contents of this website.

### Watch the following video for a summary of the future recommendations:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fTnG_lSH5vE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**If you are new to the corridor study website, the following video from 2021 provides a short overview of the project and website:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/wCiXJpTPCTY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

### Project Timeline

  {{<image src="Timeline_through September.png"
  alt="Figure showing strategic corridor study schedule"
  attr="Draft corridor study schedule"
  position="full">}}


**For questions or comments please contact:**

- Ashlee McLaughlin, Planning Manager; amclaughlin@ccrpc.org; (217) 819-4077
- JD McClanahan, Planner II; JMcClanahan@ccrpc.org


