---
title: "Emergency Services"
draft: false
weight: 60
---


It is critical to document how emergency service providers currently utilize the
roadways and service the area when assessing the existing conditions and
considering future improvements.

## Fire Protection
The City of Urbana provides fire protection within their corporate limits with
four fire stations. The Cities of Urbana and Champaign jointly provide fire
protection services for University of Illinois properties, based on an
Intergovernmental Agreement for Fire Protection Services between the University
of Illinois and the Cities of Champaign and Urbana. 

The fire stations closest to the study area are located at 400 South Vine Street
and 1105 West Gregory Drive. There are eighteen fire hydrants located along the
Florida Avenue corridor between Goodwin Avenue and Anderson Street, managed by
the Illinois American Water Company and the University of Illinois.

<iframe src="https://maps.ccrpc.org/facs-ec-fire/"
  width="100%" height="400" allowfullscreen="true">
  Map showing existing fire station closest to the Florida Avenue Corridor Study Area of Influence.
</iframe>

## Police Service 
Police protection is provided to the area of influence by the Urbana Police
Department and University of Illinois Police Department. The Urbana Police
Department provides police services within the Urbana corporate limits. The
University of Illinois Police jurisdiction boundary is visible in the map below.

<iframe src="https://maps.ccrpc.org/facs-ec-police/"
  width="100%" height="400" allowfullscreen="true">
  Map showing existing police service within the Florida Avenue Corridor Study Area of Influence.
</iframe>